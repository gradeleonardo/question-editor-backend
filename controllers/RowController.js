const express = require('express');
const rowRouter = express.Router();
const mongoose = require('mongoose');
const upload = require('../helpers/storage');

const Row = require('../models/Row');
const Question = require('../models/Question');

rowRouter.route('/:id')
	.get((req, res, next) => {
		Row
			.findById(req.params.id)
			.then(result => res.json(result));
	})
	.put((req, res, next) => {
		Row
			.findByIdAndUpdate(req.params.id, req.body, { new: true })
			.then(row => res.json(row));
	})
	.delete((req, res, next) => {
		Row.findById(req.params.id)
			.then(row => {
				Question.findById(row._questionId)
					.then(question => {
						question.rows = question.rows.filter(rId => String(rId) != String(row._id));
						question.save()
							.then(q => Row.findByIdAndDelete(row._id))
							.then(() => res.sendStatus(204));
					})
			})
	});

rowRouter.route('/:id/img')
	.post(upload.single('img'), (req, res, next) => {
		Row.findById(req.params.id)
			.then(row => {
				row.imgUrl = process.env.SERVER_URL + req.file.path;
				row.save()
					.then(savedRow => {
						res.json(savedRow);
					});
			});
	});

module.exports = rowRouter;