const express = require('express');
const columnRouter = express.Router();
const mongoose = require('mongoose');
const upload = require('../helpers/storage');

const Column = require('../models/Column');
const Question = require('../models/Question');

columnRouter.route('/:id')
	.get((req, res, next) => {
		Column
			.findById(req.params.id)
			.then(result => res.json(result));
	})
	.put((req, res, next) => {
		Column
			.findByIdAndUpdate(req.params.id, req.body, { new: true })
			.then(Column => res.json(Column));
	})
	.delete((req, res, next) => {
		Column.findById(req.params.id)
			.then(column => {
				Question.findById(column._questionId)
					.then(question => {
						question.columns = question.columns.filter(rId => String(rId) != String(column._id));
						question.save()
							.then(q => Column.findByIdAndDelete(column._id))
							.then(() => res.sendStatus(204));
					})
			})
	});

columnRouter.route('/:id/img')
	.post(upload.single('img'), (req, res, next) => {
		Column.findById(req.params.id)
			.then(col => {
				col.imgUrl = process.env.SERVER_URL + req.file.path;
				col.save()
					.then(savedCol => {
						res.json(savedCol);
					});
			});
	});

module.exports = columnRouter;