const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Row = require('./Row');
const Column = require('./Column');

const QuestionSchema = new Schema({
	title: String,
	rows: [{ type: Schema.Types.ObjectId, ref: 'Row' }],
	columns: [{ type: Schema.Types.ObjectId, ref: 'Column' }]
});
;

module.exports = mongoose.model('Question', QuestionSchema);