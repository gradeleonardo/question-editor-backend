const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const RowSchema = new Schema({
	imgUrl: String,
	label: String,
	_questionId: { type: Schema.Types.ObjectId, ref: 'Question' }
});

module.exports = mongoose.model('Row', RowSchema);