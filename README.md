# question editor backend

## How to Run the Task
`npm i && npm start`

If you would like to setup a different mongodb connection, create a `.env` file in the root directory and edit its contents to the following environment variables:

``` 
DB_USER=yourdbuser
DB_PWD=yourdbpwd
DB_HOST=yourdbhost
DB_PORT=yourdbport
DB_DATABASE=yourdbname
SERVER_URL=yourserverurl
```

## Task Description

- Ability to select images from the hard drive for every row and column
- Ability to set labels for every row and column
- Ability to add new rows and columns
- Ability to remove rows and columns

- A statistics pane on the right:
    - Amount of rows created
    - Amount of columns created
    - Amount of images uploaded
    - The string length of the longest label
    - The string length of the shortest label
- Optional: Animations (for example when adding a row or column)