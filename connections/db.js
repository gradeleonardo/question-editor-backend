const mongoose = require('mongoose');

const dbUser = process.env.DB_USER;
const dbPwd = process.env.DB_PWD;
const dbHost = process.env.DB_HOST;
const dbPort = process.env.DB_PORT;
const dbDatabase = process.env.DB_DATABASE;

const uri = `mongodb://${dbUser}:${dbPwd}@${dbHost}:${dbPort}/${dbDatabase}`;

mongoose.connect(uri, { useNewUrlParser: true })
	.then(() => console.log('Connected to the DB'))
	.catch(err => console.log(err));