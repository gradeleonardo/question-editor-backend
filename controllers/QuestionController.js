const express = require('express');
const questionRouter = express.Router();
const mongoose = require('mongoose');

const Question = require('../models/Question');
const Row = require('../models/Row');
const Column = require('../models/Column');

questionRouter.route('')
	.get((req, res, next) => {
		Question.find()
			.populate('rows')
			.populate('columns')
			.then(results => res.json(results));
	})
	.post((req, res, next) => {
		Question.create({
			title: req.body.title
		})
			.then(question => {
				res.json(question);
			});
	});

questionRouter.route('/:questionId')
	.get((req, res, next) => {
		Question.findById(req.params.questionId)
			.populate('rows')
			.populate('columns')
			.then(results => res.json(results));
	})
	.put((req, res, next) => {
		Question.findByIdAndUpdate(req.params.questionId, req.body, { new: true })
			.then(question => res.json(question));
	})
	.delete((req, res, next) => {
		// need to delete rows and columns
		Question.findByIdAndDelete(req.params.questionId)
			.then(() => res.sendStatus(204));
	});

// ROWS
questionRouter.route('/:questionId/rows')
	.get((req, res, next) => {
		Row.find({ questionId: req.params.questionId })
			.then(rows => res.json(rows));
	})
	.post((req, res, next) => {
		const questionId = req.params.questionId;
		const row = new Row({
			label: req.body.label,
			_questionId: questionId
		});
		row.save()
			.then(savedRow => {
				Question.update({ _id: questionId }, { $push: { rows: savedRow._id } })
					.then(() => res.json(savedRow));
			});
	});

// COLUMNS
questionRouter.route('/:questionId/columns')
	.get((req, res, next) => {
		Column.find({ questionId: req.params.questionId })
			.then(columns => res.json(columns));
	})
	.post((req, res, next) => {
		const questionId = req.params.questionId;
		const column = new Column({
			label: req.body.label,
			_questionId: questionId
		});
		column.save()
			.then(savedColumn => {
				Question.update({ _id: questionId }, { $push: { columns: savedColumn._id } })
					.then(() => res.json(savedColumn));
			});
	});

module.exports = questionRouter;