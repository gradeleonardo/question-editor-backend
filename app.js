const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const a = require('dotenv').config();
const app = express();
const db = require('./connections/db');
const QuestionController = require('./controllers/QuestionController');
const RowController = require('./controllers/RowController');
const ColumnController = require('./controllers/ColumnController');

app.use('/uploads', express.static('uploads'));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/questions', QuestionController);
app.use('/rows', RowController);
app.use('/columns', ColumnController);

module.exports = app;